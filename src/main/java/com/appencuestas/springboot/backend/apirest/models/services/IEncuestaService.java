package com.appencuestas.springboot.backend.apirest.models.services;

import java.util.List;

import com.appencuestas.springboot.backend.apirest.models.entity.Encuesta;

public interface IEncuestaService {

	public List<Encuesta> findAll();
}
