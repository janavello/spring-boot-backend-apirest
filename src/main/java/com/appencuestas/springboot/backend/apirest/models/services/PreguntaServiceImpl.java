package com.appencuestas.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.appencuestas.springboot.backend.apirest.models.dao.IPreguntaDao;
import com.appencuestas.springboot.backend.apirest.models.entity.Pregunta;

@Service
public class PreguntaServiceImpl implements IPreguntaService{

	@Autowired
	private IPreguntaDao preguntaDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<Pregunta> findAll(){
		
		return (List<Pregunta>) preguntaDao.findAll();
	}
	
}
