package com.appencuestas.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.appencuestas.springboot.backend.apirest.models.entity.Encuesta;
import com.appencuestas.springboot.backend.apirest.models.services.IEncuestaService;

@RestController
@RequestMapping("/api")

public class EncuestaRestController {

	@Autowired
	private IEncuestaService encuestaService;
	
	@GetMapping("/encuestas")
	public List<Encuesta> index() {
		
		return encuestaService.findAll();
	}
}
