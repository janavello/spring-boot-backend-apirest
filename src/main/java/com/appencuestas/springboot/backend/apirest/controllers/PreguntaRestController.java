package com.appencuestas.springboot.backend.apirest.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.appencuestas.springboot.backend.apirest.models.entity.Pregunta;
import com.appencuestas.springboot.backend.apirest.models.services.IPreguntaService;

@RestController
@RequestMapping("/api")
public class PreguntaRestController {

	@Autowired
	private IPreguntaService preguntaService;
	
	@GetMapping("/preguntas")
	public List<Pregunta> index() {
		
		return preguntaService.findAll();
	}
}
