package com.appencuestas.springboot.backend.apirest.models.services;

import java.util.List;

import com.appencuestas.springboot.backend.apirest.models.entity.Pregunta;

public interface IPreguntaService {

	public List<Pregunta> findAll();
}
