package com.appencuestas.springboot.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.appencuestas.springboot.backend.apirest.models.dao.IEncuestaDao;
import com.appencuestas.springboot.backend.apirest.models.entity.Encuesta;

@Service
public class EncuestaServiceImpl implements IEncuestaService {

	@Autowired
	private IEncuestaDao encuestaDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<Encuesta> findAll(){
		
		return (List<Encuesta>) encuestaDao.findAll();
	}
	
}
