package com.appencuestas.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.appencuestas.springboot.backend.apirest.models.entity.Encuesta;

public interface IEncuestaDao extends CrudRepository<Encuesta,Long> {
	
}